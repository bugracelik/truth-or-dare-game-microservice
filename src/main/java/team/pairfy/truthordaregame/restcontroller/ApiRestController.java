package team.pairfy.truthordaregame.restcontroller;


import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import team.pairfy.truthordaregame.servis.ApiService;

@Component
@RestController
public class ApiRestController {

    @Autowired
    ApiService apiService;

    @GetMapping("getTruthQuestion/{level}")
    public String getTruthQuestion(@PathVariable Integer level) throws JsonProcessingException {
        return apiService.getMyTruthQuestion(level);
    }

    @GetMapping("getDareQuestion/{level}")
    public String getDareQuestion(@PathVariable Integer level) throws JsonProcessingException {
        return apiService.getDareQuestion(level);
    }

    @GetMapping("getTruthQuestion/api-key:{key}/{level}")
    public String getTruthQuestion(@PathVariable String key, @PathVariable Integer level) throws JsonProcessingException {
        return apiService.getTruthQuestion(key, level);
    }

    @GetMapping("getDareQuestion/api-key:{key}/{level}")
    public String getDareQuestion(@PathVariable String key, @PathVariable Integer level) throws JsonProcessingException {
        return apiService.getMyDareQuestion(key, level);
    }


}
