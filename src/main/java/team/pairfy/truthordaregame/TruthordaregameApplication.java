package team.pairfy.truthordaregame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TruthordaregameApplication {

    public static void main(String[] args) {
        SpringApplication.run(TruthordaregameApplication.class, args);
    }

}
