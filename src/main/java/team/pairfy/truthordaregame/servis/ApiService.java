package team.pairfy.truthordaregame.servis;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import team.pairfy.truthordaregame.mapper.ApiKeyMapper;
import java.util.List;

@Component
public class ApiService {

    @Autowired
    JdbcTemplate jdbcTemplate;


    public String getMyTruthQuestion(Integer level) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        final String urlForTruthApi = String.format("http://pairfy.team:6060/random/%d", level);
        return restTemplate.getForObject(urlForTruthApi, String.class);
    }

    public String getDareQuestion(Integer level) {
        RestTemplate restTemplate = new RestTemplate();
        final String urlForDareApi = String.format("http://pairfy.team:6061/random/%d", level);
        return restTemplate.getForObject(urlForDareApi, String.class);
    }

    public String getTruthQuestion(String key, Integer level) {
        if (isValidKey(key)) {
            String urlForDareApi = String.format("http://pairfy.team:6060/random/%d", level);
            return new RestTemplate().getForObject(urlForDareApi, String.class);
        }
        return "{\"message\": \" Geçersiz api key girdiniz:\"}";
    }

    public String getMyDareQuestion(String key, Integer level) throws JsonProcessingException {
        if (isValidKey(key)) {
            String urlForDareApi = String.format("http://pairfy.team:6061/random/%d", level);
            return new RestTemplate().getForObject(urlForDareApi, String.class);
        }
        return "{\"message\": \" Geçersiz api key girdiniz:\"}";
    }

    private boolean isValidKey(String key) {
        List<String> myValidKeys = getMyValidKeys(key);
        int i = 0;
        while (i < myValidKeys.size()) {
            if (myValidKeys.get(i).equals(key))
                return true;
            i++;
        }
        return false;
    }

    private List<String> getMyValidKeys(String key) {
        return jdbcTemplate.query("select * from api_key", new ApiKeyMapper());
    }


}
